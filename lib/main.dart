import 'dart:async';
import 'dart:developer';
import 'dart:convert';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import 'package:jose/jose.dart';
import 'package:crypto/crypto.dart';

import 'package:uni_links/uni_links.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

void main() {
  runApp(
    // For widgets to be able to read providers, we need to wrap the entire
    // application in a "ProviderScope" widget.
    // This is where the state of our providers will be stored.
    ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SIOP Demo',
      home: MyHomePage(),
    );
  }
}

// Note: MyApp is a HookWidget, from flutter_hooks.
class MyHomePage extends HookWidget {
  Widget _scaffoldBody(String? jwk) {
    return ListView(shrinkWrap: true, padding: const EdgeInsets.all(8.0), children: [
      ListTile(
        title: const Text('Name'),
        subtitle: Text('Jun Eijima'),
      ),
      ListTile(
        title: const Text('Email'),
        subtitle: Text('j-eijima@nri.co.jp'),
      ),
      ListTile(
        title: const Text('Image Uri'),
        subtitle: Text('http://gabunomi.net/j-eijima.png'),
      ),
      ListTile(
        title: const Text('JWK'),
        subtitle: Text('$jwk'),
      ),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return useProvider(jwkProvider).when(
      data: (jwk) {
        return ProviderListener(
            provider: appLinkProvider,
            onChange: (context, uri) {
              if (uri == null) return;
              showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text('Auth Request'),
                      content: Text('${uri.toString()}'),
                      actions: [
                        TextButton(
                            child: Text('Allow'),
                            onPressed: () {
                              launch(createSiopResponse(uri as Uri?, jwk).toString());
                              Navigator.pop(context);
                            }),
                        TextButton(
                            child: Text('Cancel'),
                            onPressed: () => Navigator.pop(context)),
                      ],
                    );
                  });
            },
            child: Scaffold(
              appBar: AppBar(title: Text('SIOP Demo Home Page')),
              body: _scaffoldBody(jwk),
            ));
      },
      loading: () => const CircularProgressIndicator(),
      error: (error, stackTrace) => Text('$error, $stackTrace'),
    );
  }
}

class AppLink extends StateNotifier<Uri?> {
  AppLink() : super(null) {
    _getInitialUri();

    _sub = uriLinkStream.listen((Uri? uri) {
      log('got uri: $uri');
      state = uri;
    }, onError: (Object err) {
      log('got err: $err');
    });
  }

  late final StreamSubscription _sub;

  void _getInitialUri() async {
    try {
      final uri = await getInitialUri();
      if (uri == null) {
        log('no initial uri');
      } else {
        log('got initial uri: $uri');
      }
      state = uri;
    } on PlatformException {
      // Platform messages may fail but we ignore the exception
      print('failed to get initial uri');
    } on FormatException catch (err) {
      log('malformed initial uri');
    }
  }

  @override
  void dispose() {
    _sub?.cancel();
    super.dispose();
  }
}

final appLinkProvider = StateNotifierProvider<AppLink, Uri?>((ref) {
  return AppLink();
});

final jwkStorage = new FlutterSecureStorage();

final jwkProvider = FutureProvider<String>((ref) async {
//  await jwkStorage.write(key: 'jwk', value: null);
  String? storedJwk = await jwkStorage.read(key: 'jwk');
  if (storedJwk != null) return storedJwk;

  final jwk = JsonWebKey.generate('RS256', keyBitLength: 2048);
  await jwkStorage.write(key: 'jwk', value: json.encode(jwk.toJson()));
  log('jwk: ${json.encode(jwk.toJson())}');
  return json.encode(jwk.toJson());
});

Uri createSiopResponse(Uri? uri, String jwk) {
  final scope = uri?.queryParameters['scope'];
  final responseType = uri?.queryParameters['response_type'];
  final clientId = uri?.queryParameters['client_id'];

  final now = DateTime.now().millisecondsSinceEpoch ~/ 1000;
  final subJwk = {
    "kty": "RSA",
    "n": json.decode(jwk)["n"],
    "e": json.decode(jwk)["e"],};

  final claims = new JsonWebTokenClaims.fromJson({
    "iss": "https://self-issued.me",
    "sub_jwk": subJwk,
    "sub": base64UrlEncode(sha256.convert(utf8.encode(json.encode(subJwk))).bytes),
    "aud": clientId,
    "exp": now + 1000,
    "iat": now,
    "nonce": getNonce(12),
    "name": "Jun Eijima",
    "email": "j-eijima@nri.co.jp",
    "picture": "http://gabunomi.net/j-eijima.png",
  });
  final builder = new JsonWebSignatureBuilder();
  builder.jsonContent = claims.toJson();
  builder.addRecipient(JsonWebKey.fromJson(json.decode(jwk)));

  final jws = builder.build().toCompactSerialization();
  log(jws);
  return Uri.parse('$clientId#id_token=$jws');
}

String getNonce(int length) {
  const _randomChars = "_ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const _charsLength = _randomChars.length;

  final rand = new math.Random.secure();
  final codeUnits = new List.generate(
    length,
        (index) {
      final n = rand.nextInt(_charsLength);
      return _randomChars.codeUnitAt(n);
    },
  );
  return new String.fromCharCodes(codeUnits);
}